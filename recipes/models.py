from django.db import models
from django.conf import settings

# Create your models here.
USER_MODEL = settings.AUTH_USER_MODEL

class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField(max_length=200)
    description = models.TextField(max_length=200)
    created_on = models.DateTimeField(auto_now_add=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title


class Author(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    date_of_birth = models.DateField(null=True, blank=True)
    date_of_death = models.DateField('Died', null=True, blank=True)

    class Meta:
        ordering = ['last_name', 'first_name']

class RecipeStep(models.Model):
    step_number = models.CharField(max_length=100)
    instruction = models.TextField()
    recipe = models.ForeignKey(
        "Recipe",
        related_name="steps",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = [
            "step_number"
        ]

class Ingredient(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        "Recipe",
        related_name="ingredients",
        on_delete=models.CASCADE,
    )
    class Meta:
        ordering = [
            "food_item",
        ]
