# Generated by Django 4.2 on 2023-04-25 19:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("recipes", "0004_alter_recipe_author"),
    ]

    operations = [
        migrations.CreateModel(
            name="RecipeStep",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("step_number", models.CharField(max_length=100)),
                ("instruction", models.TextField()),
                (
                    "recipe",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="steps",
                        to="recipes.recipe",
                    ),
                ),
            ],
            options={
                "ordering": ["step_number"],
            },
        ),
    ]
