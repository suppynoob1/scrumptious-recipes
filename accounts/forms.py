from django import forms

class SignUpForm(forms.Form):
    username = forms.CharField(max_length=30)
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    password = forms.CharField(
        max_length=30,
        widget=forms.PasswordInput,
    )
    password_c = forms.CharField(
        max_length = 30,
        widget=forms.PasswordInput,
    )

class LoginForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(
        max_length=30,
        widget=forms.PasswordInput,
    )
